const animalPrototypes = {
	getName: function() {
		return `This animal name is ${this.name}`;
	},
	getGroup: function() {
		return `This animal belongs to group ${this.group}`;
	}
};

const Animal = function() {
	this.group = null;
};

var ant = new Animal();
ant = Object.create(animalPrototypes);
ant.group = "invertebrates";
ant.name = "Ant";
ant.predator = false;

console.log(ant);
console.log(ant.getName());
console.log(ant.getGroup());
