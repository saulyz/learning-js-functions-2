const Animal = function(group, name, predator) {
	this.group = group;
	this.name = name;
	this.predator = predator;
};

Animal.prototype = {
	getName: function() {
		return `This animal name is ${this.name}`;
	},
	getGroup: function() {
		return `This animal belongs to group ${this.group}`;
	}
};

const ant = new Animal("invertebrates", "Ant", false);
const fly = new Animal("invertebrates", "Fly", false);

console.log(ant);
console.log(ant.getName());
console.log(ant.getGroup());
console.log(fly);
console.log(fly.getName());
console.log(fly.getGroup());
