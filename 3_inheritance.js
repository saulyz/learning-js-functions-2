const Animal = function(group, name, predator) {
	this.group = group;
	this.name = name;
	this.predator = predator;
};

Animal.prototype = {
	getName: function() {
		return `This animal name is ${this.name}`;
	},
	getGroup: function() {
		return `This animal belongs to group ${this.group}`;
	}
};

function Ant(group, name, predator) {
	Animal.call(this, group, name, predator);
}
Ant.prototype = Object.create(Animal.prototype);

const ant1 = new Ant("invertebrates", "Ant A", false);
const ant2 = new Ant("invertebrates", "Ant B", false);

console.log(ant1);
console.log(ant1.getName());
console.log(ant1.getGroup());
console.log(ant2);
console.log(ant2.getName());
console.log(ant2.getGroup());
