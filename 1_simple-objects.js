// group;
// name;
// predator;

// getName();

function Animal(group, name, predator) {
	this.group = group;
	this.name = name;
	this.predator = predator;
	this.getName = function() {
		return `This animal name is ${this.name}`;
	};
}

const ant = new Animal("invertebrates", "Ant", false);

console.log(ant);
console.log(ant.getName());
