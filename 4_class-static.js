class Animal {
	constructor(group, name, predator) {
		this.group = group;
		this.name = name;
		this.predator = predator;

		console.log(Animal.getAnnouncement());
	}

	getName() {
		return `This animal name is ${this.name}`;
	}

	getGroup() {
		return `This animal belongs to group ${this.group}`;
	}

	static getAnnouncement() {
		return `Announcement from class`;
	}
}

const ant = new Animal("invertebrates", "Ant", false);

console.log(ant);
console.log(ant.getName());
console.log(ant.getGroup());
console.log("after", Animal.getAnnouncement());
