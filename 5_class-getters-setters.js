class Animal {
	constructor(group, name, predator) {
		this.group = group;
		this.name = name;
		this.predator = predator;
	}

	get name() {
		return this._name;
	}

	set name(name) {
		this._name = name;
		console.warn("using setter for name");
	}

	getName() {
		return `This animal name is ${this.name}`;
	}

	getGroup() {
		return `This animal belongs to group ${this.group}`;
	}
}

const ant = new Animal("invertebrates", "Ant", false);

console.log(ant);
console.log(ant.getName());
console.log(ant.getGroup());

ant.name = "another Ant";
console.log(ant.getName());
