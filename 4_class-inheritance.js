class Animal {
	constructor(group, name, predator) {
		this.group = group;
		this.name = name;
		this.predator = predator;
	}

	getName() {
		return `This animal name is ${this.name}`;
	}

	getGroup() {
		return `This animal belongs to group ${this.group}`;
	}
}

class Ant extends Animal {
	constructor(group, name, predator, family) {
		super(group, name, predator);
		this.family = family;
	}

	getFamily() {
		return `This animal belongs to family of ${this.family}`;
	}
}

const ant = new Ant("invertebrates", "Ant", false, "insect");

console.log(ant);
console.log(ant.getName());
console.log(ant.getGroup());
console.log(ant.getFamily());
