class Animal {
	constructor(group, name, predator) {
		this.group = group;
		this.name = name;
		this.predator = predator;
	}

	getName() {
		return `This animal name is ${this.name}`;
	}

	getGroup() {
		return `This animal belongs to group ${this.group}`;
	}
}

const ant = new Animal("invertebrates", "Ant", false);

console.log(ant);
console.log(ant.getName());
console.log(ant.getGroup());
